# HRI SOFTWARE 

This package contains instructions for installing and using software useful to develop HRI applications.

## INSTALL 

### DOCKER INSTALLATION 

* See [docker/README](docker/README.md)


Note: some old instructions for full installation in OS
and in [scripts/README](scripts/README.md)



## TEST


Run the docker container

        cd hri_software/docker
        ./run.bash [X11|nvidia|vnc]
        
Note: use nvidia-docker and nvidia runtime if you have nvidia graphic cards.
Use vnc (connect through browser at http://localhost:3000) if you have issues with graphical views.

Enter the container

    docker exec -it pepperhri tmux a

Run each set of commands in a different terminal.
If you are using `tmux` in docker (default configuration), 
use `CTRL-b c` to create new tmux windows.

### NAOqi

* Run NAOqi

        cd /opt/Aldebaran/naoqi-sdk-2.5.7.1-linux64
        ./naoqi

* Run `pepper_tools` scripts

        cd ~/src/pepper_tools/memory
        python read.py

        cd ~/src/pepper_tools/say
        python say.py --sentence "Hello"


### Choregraphe

* Run Choregraphe

        cd /opt/Aldebaran/choregraphe-suite-2.5.10.7-linux64
        ./choregraphe

    Enter license key available here https://support.old.unitedrobotics.group/en/support/solutions/articles/80001024221-pepper-2-5-downloads

    Change robot type through "Edit / Preferences / Virtual Robot"

    In the same tab, check port used for the virtual robot (NAOqi state: Running on port XXXXX)
    
    Connect to naoqi server <host:port> using the specified port.

* Run `pepper_tools` scripts

        cd ~/src/pepper_tools/say
        python say.py --sentence "Hello"


### qibullet

Copy `test_qibullet.py` in some folder of `playground` to be reached from the container.

* Run qibullet

        cd playground/....
        python test_qibullet.py



## Development

Use `playground` folder to write programs for your robot. Do not directly edit files in `pepper_tools`, as it will prevent future updates.


### Connection to Pepper robot

`pepper_tools` scripts connect to IP address stored in environment variable `PEPPER_IP`.

In the docker container, set robot's IP address

    export PEPPER_IP=<robot_IP>

Note: you have to execute this command in every terminal you want to use to connect to the robot.



### pepper_tools program

* Host OS

        cd ~/playground
        <your_preferred_editor> sayhello.py

            import os, sys

            sys.path.append(os.getenv('PEPPER_TOOLS_HOME')+'/cmd_server')

            import pepper_cmd
            from pepper_cmd import *

            begin()

            pepper_cmd.robot.say('Hello')

            end()


* Docker container (simulation mode)

    Execution will be emulated in the local naoqi system (no actions/sensing will be actually executed)

    Terminal 1:

        cd /opt/Aldebaran/naoqi-sdk-2.5.5.5-linux64
        ./naoqi

    Terminal 2:

        cd ~/playground
        python sayhello.py



* Docker container (robot mode)

    Execution will run on the robot

    Terminal 1:

        export PEPPER_IP=<robot_IP>

        cd ~/playground
        python sayhello.py


## Web applications

Put your web application in a folder (e.g., `$HOME/playground/html`)

Run nginx web server pointing to that folder (from the OS system)

    cd hri_software/docker
    ./run_nginx.bash $HOME/playground/html
    
Note: no other web server on port 80 should be running.

Connect with a browser from the local host

    http://localhost

or from a different host specifying the server IP or name

    http://<Server IP>


## MODIM

Make sure a naoqi server is running.

If naoqi server is not running on the default port 9559, use the following in the terminal where you'll run modim server

    export PEPPER_PORT=<naoqi server port>

Run in the container

    cd ~/src/modim/src/GUI
    python ws_server.py -robot pepper

Check for a green text saying 

    Connected to robot <IP>:<port>
    
Run the web server (make sure no other web server is running on port 80 before).

From OS system

    cd hri_software/docker
    ./run_nginx.bash $HOME/src/Pepper/modim/demo/sample
    
Open a browser to `http://localhost` or `http://<server IP>` for remote connections.

You should see an image and a green OK in the top right that confirms connection with MODIM server.

Run a MODIM demo in the docker container

    cd ~/src/modim/demo/sample/scripts
    python demo1.py
    
A Start button will appear in the web page and the demo will start.

### Run a customized MODIM demo

Create a MODIM demo in a new folder (e.g., `$HOME/playground/mymodimdemo`)

Run the MODIM server as explained above

Run the web server pointing at your demo folder.

From OS system

    cd hri_software/docker
    ./run_nginx.bash $HOME/playground/mymodimdemo

Use the browser to interact with the robot as explained above.



